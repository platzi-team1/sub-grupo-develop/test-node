const app = require('../../app');
const request = require('supertest');

/******* Tareas a comprobar */ 
// probar los header
// probar diferentes tipos autenticacion
// probar un get query params  
// probar capacidad o rendimeinto en las peticiones


describe('GET /traerDatosPersonalesTest', () => {
    
    // Comprobar status
    test('should respond with a 200 status code', async () => {
        const response = await request(app).get('/api/traerDatosPersonalesTest').send({});
        expect(response.statusCode).toBe(200);
        //.expect('Content-Type', /json/)
    }); 
    
    // Comprobar que la respuesta sea un objeto
    test('should respond with Object', async () => {
        const response = await request(app).get('/api/traerDatosPersonalesTest').send();
        expect(response.body).toBeInstanceOf(Object);
        //.expect('Content-Type', /json/)
    }); 
    
});


describe('POST /llevarDatosPersonalesTest', () => {

    // when title and description is missing
    describe('Cuando falta el titulo y la descripcion', () => {
        // Should respond with a 400 status code
        test('Deberia responder con un codigo de estado 400', async () => {
            const response = await request(app).post('/api/llevarDatosPersonalesTest').send();
            expect(response.statusCode).toBe(400);
        }); 
    });



    describe('Comprobacion del estatus, content-type y definicion de variables', () => {
        // should respond with status 200
        test('Deberia se un estatus 200', async () => {
            const response = await request(app).post('/api/llevarDatosPersonalesTest').send({
                title: "test",
                description: "test description"
            });
            expect(response.statusCode).toBe(200);
        }); 
        
        // should respond with content-type of application/json
        test('deberia tener content-type: application/json en el header', async () => {
            const response = await request(app).post('/api/llevarDatosPersonalesTest').send({
                title: "test",
                description: "test description"
            });
            expect(response.headers["content-type"]).toEqual(expect.stringContaining("json") );
        }); 

        // should respond with a json object containing the new task with on id
        test('deberia estar definido en la respuesta el id', async () => {
            const response = await request(app).post('/api/llevarDatosPersonalesTest').send({});
            expect(response.body.id).toBeDefined();
        }); 

    });

    
});


