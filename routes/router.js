'use strict'

const express = require('express');
const datosPersonalesController = require('../controllers/datosPersonales/datosPersonales');
const router = express.Router();

router.get("/", (req, res) => res.status(200).json({ msg: "OK" }));



router.get("/traerDatosPersonalesTest", datosPersonalesController.testGet);
router.post("/llevarDatosPersonalesTest", datosPersonalesController.testPost);

/*router.post("/PostSolicitud/:email", Controller.PostSolicitud);
router.post("/postCreate", Controller.creatematter);
router.get("/GetInfo/:matterId/:exportId", Controller.GetInfo);
router.get("/GetDrive/:email", Controller.GetDrive);*/

module.exports = router;