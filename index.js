const app = require('./app');
const port = 6000;

// Crear servidor y ponerme a escuchar peticiones HTTP
app.listen(port, () => {
    console.log('Servidor corriendo en http://localhost:'+port);
});

  