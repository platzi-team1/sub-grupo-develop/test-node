'use strict'

const request = require('supertest');
const { v4: uuidv4, v4 } = require('uuid');

var controller = {

    testPost: (req, res) => {
        const { title, description } = req.body;

        if( title == undefined || description == undefined || title == null || description == null || title == "" || description == ""){
            return res.sendStatus(400);
        }
    
        return res.status(200).send({
            title,
            description,
            id: v4()
        });
    },

    testGet: (req, res) => {
        return res.status(200).send({
            message: 'Soy la acción test de mi controlador de articulos'
        });
    },


};  // end controller

module.exports = controller;

